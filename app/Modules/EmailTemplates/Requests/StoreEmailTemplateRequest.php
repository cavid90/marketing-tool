<?php

namespace App\Modules\EmailTemplates\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StoreEmailTemplateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ? intval($this->id) : 0;
        return [
            'name' => [
                'required',
            ],
            'code' => [
                'required',
                'unique:email_templates,code,'.$id,
            ],
            'subject' => [
                'required',
            ],
            'template' => [
                'required',
            ],
        ];
    }
}
