<?php

namespace App\Modules\Customers\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    use HasFactory;

    /**
     * Fillable fields
     * @var string[]
     */
    protected $fillable = [
        'email',
        'firstname',
        'lastname',
        'sex',
        'birthday',
    ];

    public function groups()
    {
        return $this->belongsToMany(\App\Modules\Groups\Models\Groups::class, 'customer_group', 'customer_id', 'group_id');
    }

    public function customerGroups()
    {
        return $this->hasMany(\App\Modules\CustomersGroups\Models\CustomerGroup::class,  'customer_id');
    }

    public function synchronizeGroups($id,$groups)
    {
        $customer = self::find($id);
        $customer->customerGroups()->delete();
        $customer->customerGroups()->saveMany($groups);
        return true;
    }
}
