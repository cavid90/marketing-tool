<div class="form-group">
    <label>Template unique code (no spaces, with letters and underscore. Example: mass_email)</label>
    <input type="text" class="form-control" name="code" value="{{$template ? $template->code :  old('code') }}">
</div>
<div class="form-group">
    <label>Template name</label>
    <input type="text" class="form-control" name="name" value="{{$template ? $template->name :  old('name') }}">
</div>
<div class="form-group">
    <label>Subject (<b>Example: E-mail from {=site=}</b>)</label>
    <input type="text" class="form-control" name="subject" value="{{$template ? $template->subject :  old('subject') }}">
</div>
<div class="form-group">
    <label>Text (<b>Example: Hi, dear {=user=}</b>)</label>
    <textarea class="form-control" name="template" rows="6">{!! $template ? $template->template :  old('template') !!}</textarea>
</div>
