<?php

namespace App\Modules\Groups\Requests;

use Illuminate\Foundation\Http\FormRequest;


class GroupMailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'delay' => ['required', 'boolean'],
            'template_id' => ['required'],
        ];

        if($this->get('delay') == 1){
            $rules['time'] = ['required', 'numeric', 'min:1'];
        }

        return $rules;
    }

}
