<?php

namespace App\Jobs;

use App\Mail\EmailForQueue;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $customers = $this->data['customers'];
        $template = $this->data['template'];
        $subject = $this->data['subject'];

        foreach ($customers as $customer) {
            # Set email template variables
            $user = $customer->firstname.' '.$customer->lastname;
            $site = config('app.name');

            preg_match_all('/{=(\w+)=}/', $subject, $matches);
            $newSubject = $subject;
            foreach ($matches[0] as $index => $var_name) {
                if (isset(${$matches[1][$index]})) {
                    $newSubject = str_replace($var_name, ${$matches[1][$index]}, $newSubject);
                }
            }

            ####################
            preg_match_all('/{=(\w+)=}/', $template, $matches);
            $newTemplate = $template;
            foreach ($matches[0] as $index => $var_name) {
                if (isset(${$matches[1][$index]})) {
                    $newTemplate = str_replace($var_name, ${$matches[1][$index]}, $newTemplate);
                }
            }

            $emailObject = new EmailForQueue($newTemplate, $newSubject);
            Mail::to($customer->email)->send($emailObject);
        }

    }
}
