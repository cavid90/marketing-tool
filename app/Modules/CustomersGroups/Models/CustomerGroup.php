<?php

namespace App\Modules\CustomersGroups\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerGroup extends Model
{
    use HasFactory;

    protected $table = 'customer_group';
    /**
     * Disable timestamps
     * @var bool
     */
    public $timestamps = false;

    /**
     * Fillable fields
     * @var string[]
     */
    protected $fillable = [
        'customer_id',
        'group_id',
    ];

    public function customer() {
        return $this->belongsTo('App\Modules\Customers\Models\Customers', 'customer_id');
    }

    public function group() {
        return $this->belongsTo('App\Modules\Groups\Models\Groups', 'group_id');
    }
}
