<?php

namespace App\Modules\Customers\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Customers\Models\Customers;
use App\Modules\Customers\Requests\StoreCustomerRequest;
use App\Modules\CustomersGroups\Models\CustomerGroup;
use App\Modules\Groups\Models\Groups;
use Illuminate\Support\Facades\Redirect;

class CustomersController extends Controller
{

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customers = Customers::with('groups')->get();
        return view('customers.index', [
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $groups = Groups::all();
        return view('customers.create', [
            'groups' => $groups
        ]);
    }

    /**
     * @param StoreCustomerRequest $request
     */
    public function store(StoreCustomerRequest $request)
    {
        $request->validated();

        $data = $request->safe()->except(['customerGroups']);
        $create = Customers::create($data);

        if($create) {
            # Update customer groups
            $id = $create->id;
            $synchronizedGroups = $request->mapCustomerGroups($id);
            Customers::synchronizeGroups($id, $synchronizedGroups);

            return Redirect::to(route('customers.index'))->with('message', 'Customer created');
        }
        Redirect::to(route('customers.index'))->withErrors(['Could not create customer']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $customer = Customers::with('groups')->find($id);
        $groups = Groups::all();
        if(!$customer) {
            return Redirect::to(route('customers.index'))->withErrors(['Customer not found']);
        }
        $customerGroups = $customer->groups->map(function ($val) {
            return $val->id;
        })->toArray();

        return view('customers.edit', [
            'customer' => $customer,
            'groups' => $groups,
            'customerGroups' => $customerGroups
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(StoreCustomerRequest $request, $id)
    {
        $request->validate($request->rules());
        $data = $request->safe()->except(['customerGroups']);

        # Update customer
        $update = Customers::where('id',$id)->update($data);
        if($update) {
            # Update customer groups
            $synchronizedGroups = $request->mapCustomerGroups($id);
            Customers::synchronizeGroups($id, $synchronizedGroups);

            return Redirect::to(route('customers.edit', ['customer' => $id]))->with('message', 'Updated');
        }
        Redirect::to(route('customers.edit', ['customer' => $id]))->withErrors(['Could not update customer']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete($id)
    {
        $isDeleted = Customers::destroy($id);
        if($isDeleted) {
            return Redirect::to(route('customers.index'))->with('message', 'Deleted');
        }
        Redirect::to(route('customers.index'))->withErrors(['Could not delete customer']);
    }

}
