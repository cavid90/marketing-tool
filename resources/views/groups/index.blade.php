@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Groups') }} | <a href="{{ route('groups.create') }}">New Group</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('partials.success_error_message')
                    @if($groups)
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Operations</th>
                                </thead>
                                @foreach($groups as $group)
                                    <tr>
                                        <th>{{ $group->id }}</th>
                                        <th>{{ $group->name }}</th>
                                        <th>
                                            <a href="{{ route('groups.edit', ['group' => $group->id]) }}">Edit</a> |
                                            <a href="{{ route('groups.delete', ['group' => $group->id]) }}">Delete</a> |
                                            <a href="{{ route('groups.new_mail', ['group' => $group->id]) }}">New mail</a>
                                        </th>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
