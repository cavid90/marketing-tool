<?php

namespace App\Modules\Groups\Controllers;

use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use App\Modules\EmailTemplates\Models\EmailTemplates;
use App\Modules\Groups\Models\Groups;
use App\Modules\Groups\Requests\GroupMailRequest;
use App\Modules\Groups\Requests\StoreGroupRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class GroupsController extends Controller
{

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $groups = Groups::all();
        return view('groups.index', [
            'groups' => $groups
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('groups.create');
    }

    /**
     * @param StoreGroupRequest $request
     */
    public function store(StoreGroupRequest $request)
    {
        $request->validated();
        $data = $request->safe()->toArray();
        $create = Groups::create($data);
        if($create) {
            return Redirect::to(route('groups.index'))->with('message', 'Group created');
        }
        Redirect::to(route('groups.index'))->withErrors(['Could not create group']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $group = Groups::find($id);
        if(!$group) {
            return $this->index();
        }
        return view('groups.edit', [
            'group' => $group
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(StoreGroupRequest $request, $id)
    {
        $request->validate($request->rules());
        $data = $request->safe()->toArray();
        $update = Groups::where('id',$id)->update($data);
        if($update) {
            return Redirect::to(route('groups.edit', ['group' => $id]))->with('message', 'Updated');
        }
        Redirect::to(route('groups.edit', ['group' => $id]))->withErrors(['Could not update group']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete($id)
    {
        $isDeleted = Groups::destroy($id);
        if($isDeleted) {
            return Redirect::to(route('groups.index'))->with('message', 'Deleted');
        }
        Redirect::to(route('groups.index'))->withErrors(['Could not delete group']);
    }


    /**
     * Send email view
     *
     * @param  int  $id
     */
    public function newMail($id)
    {
        $templates = EmailTemplates::all();
        $group = Groups::findOrFail($id);
        return view('groups.new_mail', [
            'templates' => $templates,
            'group' => $group
        ]);
    }

    /**
     * @param GroupMailRequest $request
     */
    public function sendMail(GroupMailRequest $request, Groups $group)
    {
        $rules = $request->rules();
        $request->validate($rules);
        $data = $request->safe()->toArray();
        $template = EmailTemplates::find($data['template_id']);
        $emailData = [
            'customers' => $group->customers,
            'template' => $template->subject,
            'subject' => $template->template
        ];

        if($data['delay'] && $data['delay'] == 1 && $data['time'] > 0) {
            $newMailJob = (new SendEmail($emailData))->delay(Carbon::now()->addMinutes($data['time']));
            $this->dispatch($newMailJob);
        } else {
            SendEmail::dispatch($emailData);
        }
        return Redirect::to(route('groups.index'))->with('message', 'E-mails were sent');
    }
}
