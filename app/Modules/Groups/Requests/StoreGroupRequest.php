<?php

namespace App\Modules\Groups\Requests;

use Illuminate\Foundation\Http\FormRequest;


class StoreGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ? intval($this->id) : 0;
        return [
            'name' => [
                'required',
                'unique:groups,name,'.$id,
            ],
        ];
    }

}
