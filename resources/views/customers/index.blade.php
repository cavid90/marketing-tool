@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Customers') }} | <a href="{{ route('customers.create') }}">New Customer</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('partials.success_error_message')
                    @if($customers)
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <th>ID</th>
                                <th>E-mail</th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Sex</th>
                                <th>Birthday</th>
                                <th>Groups</th>
                                <th>Operations</th>
                                </thead>
                                @foreach($customers as $customer)
                                    <tr>
                                        <td>{{ $customer->id }}</td>
                                        <td>{{ $customer->email }}</td>
                                        <td>{{ $customer->firstname }}</td>
                                        <td>{{ $customer->lastname }}</td>
                                        <td>{{ $customer->sex == 0 ? 'Woman' : 'Man' }}</td>
                                        <td>{{ $customer->birthday }}</td>
                                        <td>
                                            @if($customer->groups)
                                                @foreach($customer->groups as $group)
                                                 {{ $group->name }},
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('customers.edit', ['customer' => $customer->id]) }}">Edit</a> |
                                            <a href="{{ route('customers.delete', ['customer' => $customer->id]) }}">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
