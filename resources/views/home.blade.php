@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Hi, '.auth()->user()->name) }}
                    <hr>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a href="{{ route('customers.index') }}">Customers</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('groups.index') }}">Groups</a>
                        </li>
                        <li class="list-group-item">
                            <a href="{{ route('email-templates.index') }}">E-mail templates</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
