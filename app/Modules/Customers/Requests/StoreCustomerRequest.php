<?php

namespace App\Modules\Customers\Requests;

use App\Modules\CustomersGroups\Models\CustomerGroup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id ? intval($this->id) : 0;
        return [
            'email' => [
                'required',
                'unique:customers,email,'.$id,
            ],
            'firstname' => 'required',
            'lastname' => 'required',
            'sex' => 'required|boolean',
            'birthday' => 'date_format:Y-m-d',
        ];
    }

    public function mapCustomerGroups($id)
    {
        $customerGroups = $this->get('customerGroups', []);
        return collect($customerGroups)->map(function ($group_id) use ($id) {
            $customerGroup = new CustomerGroup();
            $customerGroup->group_id = $group_id;
            $customerGroup->customer_id = $id;
            return $customerGroup;
        });
    }
}
