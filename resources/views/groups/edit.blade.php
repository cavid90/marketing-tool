@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit group - '.$group->name) }} | <a href="{{ route('groups.index') }}">Groups</a></div>
                <div class="card-body">
                    @include('partials.success_error_message')
                    <form action="{{ route('groups.update', ['group' => $group->id]) }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('PATCH') !!}
                        <input type="hidden" name="id" value="{{ $group->id }}">
                        @include('groups.form', ['group' => $group])
                        <div class="form-group">
                            <button class="btn btn-success btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
