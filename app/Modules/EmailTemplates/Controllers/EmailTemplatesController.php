<?php

namespace App\Modules\EmailTemplates\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\EmailTemplates\Models\EmailTemplates;
use App\Modules\EmailTemplates\Requests\StoreEmailTemplateRequest;
use Illuminate\Support\Facades\Redirect;

class EmailTemplatesController extends Controller
{

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $templates = EmailTemplates::all();
        return view('email-templates.index', [
            'templates' => $templates
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('email-templates.create');
    }

    /**
     * @param StoreEmailTemplateRequest $request
     */
    public function store(StoreEmailTemplateRequest $request)
    {
        $request->validated();
        $data = $request->safe()->toArray();
        $create = EmailTemplates::create($data);
        if($create) {
            return Redirect::to(route('email-templates.index'))->with('message', 'Email-template created');
        }
        Redirect::to(route('email-templates.index'))->withErrors(['Could not create email-template']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $template = EmailTemplates::find($id);
        if(!$template) {
            return $this->index();
        }
        return view('email-templates.edit', [
            'template' => $template
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  StoreEmailTemplateRequest  $request
     * @param  int  $id
     */
    public function update(StoreEmailTemplateRequest $request, $id)
    {
        $request->validate($request->rules());
        $data = $request->safe()->toArray();
        $update = EmailTemplates::where('id',$id)->update($data);
        if($update) {
            return Redirect::to(route('email-templates.edit', ['email_template' => $id]))->with('message', 'Updated');
        }
        Redirect::to(route('email-templates.edit', ['email_template' => $id]))->withErrors(['Could not update email-template']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function delete($id)
    {
        $isDeleted = EmailTemplates::destroy($id);
        if($isDeleted) {
            return Redirect::to(route('email-templates.index'))->with('message', 'Deleted');
        }
        Redirect::to(route('email-templates.index'))->withErrors(['Could not delete email-template']);
    }

}
