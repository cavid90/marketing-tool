@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create template') }} | <a href="{{ route('email-templates.index') }}">E-mail templates</a></div>

                <div class="card-body">
                    @include('partials.success_error_message')
                    <form action="{{ route('email-templates.store') }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('POST') !!}
                        @include('email-templates.form', ['template' => false])
                        <div class="form-group">
                            <button class="btn btn-success btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
