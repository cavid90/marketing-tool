<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## E-mail marketing MVP on Laravel

#### How to use:
* Fill env file with your credentials for database, email. Queue will be database
* Please run before starting. For adding tables to database
                                                         
```
$ composer install
$ npm install
$ npm run dev
$ php artisan migrate
```

* For listening to emails, run:
```
$ php artisan queue:work
```

And it will be ready to use. 
PS. laravel/ui with auth scaffolding was used for Authorization. The rest - everything was done by me
#### Note:
- I have used modular structure which I have seen in nestjs. Like this:
- Modules
    - Module directory. Example: Groups
        - Controllers directory and inside controllers
        - Models directory and inside models
        - Requests directory  and inside requests

