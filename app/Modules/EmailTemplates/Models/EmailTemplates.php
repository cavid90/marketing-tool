<?php

namespace App\Modules\EmailTemplates\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailTemplates extends Model
{
    use HasFactory;

    /**
     * Fillable fields
     * @var string[]
     */
    protected $fillable = [
        'name',
        'code',
        'subject',
        'template',
    ];
}
