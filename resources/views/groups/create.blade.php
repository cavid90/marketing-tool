@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create group') }} | <a href="{{ route('groups.index') }}">Groups</a></div>

                <div class="card-body">
                    @include('partials.success_error_message')
                    <form action="{{ route('groups.store') }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('POST') !!}
                        @include('groups.form', ['group' => false])
                        <div class="form-group">
                            <button class="btn btn-success btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
