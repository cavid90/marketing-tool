@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Send email to '.$group->name.' customers') }} | <a href="{{ route('groups.index') }}">Groups</a></div>

                <div class="card-body">
                    @include('partials.success_error_message')
                    <form action="{{ route('groups.send_mail', ['group' => $group]) }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('POST') !!}
                        <div class="form-group">
                            <label>E-mail template</label>
                            <select class="form-control" name="template_id">
                                @if($templates)
                                    @foreach($templates as $template)
                                        <option value="{{ $template->id }}" {{ old('template_id') }}>{{ $template->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="delay" value="0" {{ (old('delay') == 0 ? 'checked' : '') }}> Send immediately</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="delay" value="1" {{ (old('delay') == 1 ? 'checked' : '') }}> Send with delay</label>
                        </div>
                        <div class="form-group">
                            <label>Delay time in minutes</label>
                            <input type="number" min="0" class="form-control" name="time" value="{{ old('time') ? old('time') : 0 }}">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-success btn-block">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
