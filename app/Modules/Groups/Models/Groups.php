<?php

namespace App\Modules\Groups\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    use HasFactory;

    /**
     * Fillable fields
     * @var string[]
     */
    protected $fillable = [
        'name',
    ];

    public function customers()
    {
        return $this->belongsToMany(\App\Modules\Customers\Models\Customers::class, 'customer_group', 'group_id', 'customer_id');
    }
}
