<div class="form-group">
    <label>Email</label>
    <input type="email" class="form-control" name="email" value="{{ $customer ? $customer->email : old('email') }}">
</div>
<div class="form-group">
    <label>Firstname</label>
    <input type="text" class="form-control" name="firstname" value="{{$customer ? $customer->firstname :  old('firstname') }}">
</div>
<div class="form-group">
    <label>Lastname</label>
    <input type="text" class="form-control" name="lastname" value="{{$customer ? $customer->lastname :  old('lastname') }}">
</div>
<div class="form-group">
    <label>Birthday</label>
    <input type="text" class="form-control" name="birthday" value="{{$customer ? $customer->birthday :  old('birthday') }}">
</div>
<div class="form-group">
    <label>Group</label>
    <select class="form-control" name="customerGroups[]" multiple>
        @if($groups)
            @foreach($groups as $group)
                <option value="{{ $group->id }}" {{ isset($customerGroups) && in_array($group->id, $customerGroups) ? 'selected' : '' }}>{{ $group->name }}</option>
            @endforeach
        @endif
    </select>
</div>
<div class="radio">
    <label><input type="radio" name="sex" value="0" {{ $customer ? ($customer->sex == 0 ? 'checked' : '') : (old('sex') == 0 ? 'checked' : '') }}> Man</label>
</div>
<div class="radio">
    <label><input type="radio" name="sex" value="1" {{ $customer ? ($customer->sex == 1 ? 'checked' : '') : (old('sex') == 1 ? 'checked' : '') }}> Woman</label>
</div>
