@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Create customer') }} | <a href="{{ route('customers.index') }}">Customers</a></div>

                <div class="card-body">
                    @include('partials.success_error_message')
                    <form action="{{ route('customers.store') }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('POST') !!}
                        @include('customers.form', ['customer' => false])
                        <div class="form-group">
                            <button class="btn btn-success btn-block">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
