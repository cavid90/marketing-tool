<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

# Auth and dashboard
Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

# Customers
Route::resource('customers', \App\Modules\Customers\Controllers\CustomersController::class);
Route::get('/customers/delete/{customer}', [\App\Modules\Customers\Controllers\CustomersController::class, 'delete'])->name('customers.delete');

# Groups
Route::resource('groups', \App\Modules\Groups\Controllers\GroupsController::class);
Route::get('/groups/delete/{group}', [\App\Modules\Groups\Controllers\GroupsController::class, 'delete'])->name('groups.delete');
Route::get('/groups/new-mail/{group}', [\App\Modules\Groups\Controllers\GroupsController::class, 'newMail'])->name('groups.new_mail');
Route::post('/groups/send-mail/{group}', [\App\Modules\Groups\Controllers\GroupsController::class, 'sendMail'])->name('groups.send_mail');

# E-mail templates
Route::resource('email-templates', \App\Modules\EmailTemplates\Controllers\EmailTemplatesController::class);
Route::get('/email-templates/delete/{email_template}', [\App\Modules\EmailTemplates\Controllers\EmailTemplatesController::class, 'delete'])->name('email-templates.delete');
