@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('E-mail templates') }} | <a href="{{ route('email-templates.create') }}">New template</a></div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @include('partials.success_error_message')
                    @if($templates)
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Operations</th>
                                </thead>
                                @foreach($templates as $template)
                                    <tr>
                                        <th>{{ $template->id }}</th>
                                        <th>{{ $template->name }}</th>
                                        <th>
                                            <a href="{{ route('email-templates.edit', ['email_template' => $template->id]) }}">Edit</a> |
                                            <a href="{{ route('email-templates.delete', ['email_template' => $template->id]) }}">Delete</a>
                                        </th>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
